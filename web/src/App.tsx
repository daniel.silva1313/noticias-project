import React from 'react';
import { Provider } from 'react-redux';
import Layout from './components/layout';

// import NoticiaList from './components/NoticiaList';

import store from './store';

// const App = () => <Provider store={store}><NoticiaList /></Provider>;
const App = () => <Provider store={store}><Layout /></Provider>;

export default App;
