import axios from 'axios';
import * as dotenv from 'dotenv';

dotenv.config();

const { SERVER_HOST, SERVER_PORT } = process.env;
const api = axios.create({
  baseURL: (SERVER_HOST) ? `http://${SERVER_HOST}:${SERVER_PORT}` : 'http://localhost:3001',
});

export default api;
