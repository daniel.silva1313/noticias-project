import React from 'react';

import { Noticia } from '../../store/ducks/noticias/types';

import './style.scss';

interface OwnProps {
  noticia: Noticia
  onSelectedNoticia: (not: Noticia) => void
}

type Props = OwnProps
export default class NoticiaItem extends React.PureComponent<Props> {
  render() {
    const { onSelectedNoticia } = this.props;
    const { noticia } = this.props;

    const data = new Date(noticia.dataPublicacao);

    return (
      <li onClick={() => onSelectedNoticia(noticia)}>
        {noticia.titulo}
        -
        {' '}
        {`${data.getDate()}/${data.getMonth()}/${data.getFullYear()}`}
      </li>
    );
  }
}
