import React, { Component } from 'react';
import ArtigoCrud from '../ArtigoCrud';
import ArtigoList from '../ArtigoList';
import './style.scss';

class Layout extends Component {
    componentDidMount() {
    }

    render() {
        return (
          <div className="main">
            <div className="lista-artigos">
              <ArtigoList />
            </div>
            <div className="noticias">
              <ArtigoCrud />
            </div>
          </div>
        );
    }
}

export default Layout;
