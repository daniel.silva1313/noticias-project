import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { ApplicationState } from '../../store';

import * as NoticiaActions from '../../store/ducks/noticias/actions';
import { Noticia, PageMode } from '../../store/ducks/noticias/types';
import NoticiaItem from '../NoticiaItem';
import './style.scss';

interface StateProps {
  mode: PageMode
  noticias: Noticia[]
}


interface DispatchProps {
  changePageMode(mode: PageMode): void
  setCurrentNoticia(noticia: Noticia): void
  loadRequest(): void
}

type Props = StateProps & DispatchProps

class ArtigoList extends Component<Props> {
    componentDidMount() {
      const { loadRequest } = this.props;
      loadRequest();
    }

    clickHandler(not: Noticia): void{
      const { mode, changePageMode, setCurrentNoticia } = this.props;
      if (mode === PageMode.CREATE_UPDATE) {
        changePageMode(PageMode.VISUALISATION);
      }

      setCurrentNoticia(not);
    }

    render() {
      const { noticias } = this.props;
      const noticiaItems = (noticias.length > 0) ? noticias.map(not => (
        <NoticiaItem
          key={not.id}
          noticia={not}
          onSelectedNoticia={() => this.clickHandler(not)}
        />
      )) : <li>Nada a listar</li>;
        return (
          <div>
            <div>Lista de artigos</div>
            <ul>
              {noticiaItems}
            </ul>
          </div>
        );
    }
}

const mapStateToProps = (state: ApplicationState) => ({
  noticia: state.repositories.currentNoticia,
  noticias: state.repositories.data,
  mode: state.repositories.mode,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(NoticiaActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ArtigoList);
