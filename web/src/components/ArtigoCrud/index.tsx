import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { ApplicationState } from '../../store';
import { Noticia, PageMode } from '../../store/ducks/noticias/types';
import * as NoticiaActions from '../../store/ducks/noticias/actions';
import './style.scss';
import ArtigoHeader from './ArtigoHeader';


interface OwnProps {
  noticia: Noticia
}
interface StateProps {
  mode: PageMode
  noticias: Noticia[]
}


interface DispatchProps {
  changePageMode(mode: PageMode): void
  loadRequest(): void
  saveOrUpdate(noticia: Noticia): void
  setCurrentNoticia(noticia: Noticia): void
}

type Props = StateProps & DispatchProps & OwnProps

class ArtigoCrud extends Component<Props> {
  componentDidMount() {
  }

  setMode(nextMode: PageMode) {
    const { changePageMode } = this.props;
    changePageMode(nextMode);
  }

  noticiaCreateHandler() {
    const { saveOrUpdate, noticia } = this.props;
    // todo: por validações
    console.log('minha noticia para salvar', noticia);
    saveOrUpdate(noticia);
  }

  handleConteudoChande(e: any) {
    const { noticia, setCurrentNoticia } = this.props;
    const conteudo = e.target.value;
    setCurrentNoticia({ ...noticia, conteudo });
  }

  render() {
    const { noticia } = this.props;
    const { mode } = this.props;

    let conteudo = null;


    if (mode === PageMode.VISUALISATION) {
      conteudo = <div className="conteudo">{noticia.conteudo}</div>;
    } else {
      conteudo = <textarea rows={3} cols={100} className="conteudo" onChange={e => this.handleConteudoChande(e)} defaultValue={noticia.conteudo} />;
    }

    return (
      <div className="artigo">
        <div className="title">
          <ArtigoHeader onNoticiaCreate={() => this.noticiaCreateHandler()} />
        </div>
        {conteudo}
      </div>
    );
  }
}


const mapStateToProps = (state: ApplicationState) => ({
  mode: state.repositories.mode,
  noticia: state.repositories.currentNoticia,
  noticias: state.repositories.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(NoticiaActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ArtigoCrud);
