/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { ApplicationState } from '../../../store';
import { Noticia, PageMode } from '../../../store/ducks/noticias/types';
import * as NoticiaActions from '../../../store/ducks/noticias/actions';
import './style.scss';

interface OwnProps {
  onNoticiaCreate: (not: Noticia) => void
}

interface StateProps {
  noticia: Noticia
  mode: PageMode
}

interface DispatchProps {
  changePageMode(mode: PageMode): void
  removeItem(id: string): void
  setCurrentNoticia(noticia: Noticia): void
}

type Props = StateProps & DispatchProps & OwnProps


class ArtigoHeader extends React.PureComponent<Props> {
  componentDidMount() {
  }

  getBotoes(mode: PageMode) {
    const { noticia } = this.props;
    // #region botões
    const save = <a className="btn save" onClick={e => this.handleClick(e, PageMode.VISUALISATION)}>save</a>;
    const canc = <a className="btn cancel" onClick={e => this.handleClick(e, PageMode.VISUALISATION)}>cancel</a>;
    const add = <a className="btn add" onClick={e => this.handleClick(e, PageMode.CREATE_UPDATE)}>add</a>;
    const edit = <a className="btn edit" onClick={e => this.handleClick(e, PageMode.CREATE_UPDATE)}>edit</a>;
    const del = <a className="btn delele" onClick={e => this.handleClick(e, PageMode.CREATE_UPDATE)}>delete</a>;
    // #endregion

    if (mode === PageMode.VISUALISATION) {
      return (noticia.id) ? (
        <div>
          {edit}
          {del}
        </div>
) : (
  <div>
    {add}
    {del}
  </div>
);
    }
    return (
      <div>
        {save}
        {canc}
      </div>
    );
  }

  setMode(nextMode: PageMode) {
    const { changePageMode } = this.props;
    changePageMode(nextMode);
  }

  handleClick(e: any, mode: PageMode) {
    const action = e.target.text;
    const { noticia, removeItem, onNoticiaCreate } = this.props;
    console.log(action);
    if (action === 'add') {
      console.log('está assim.', noticia);
    }
    if (action === 'update' || action === 'save') {
      onNoticiaCreate(noticia);
    } else if (action === 'delete') {
      removeItem(noticia.id || '');
    }
    this.setMode(mode);
  }

  handleDateChande(e: any) {
    const { noticia, setCurrentNoticia } = this.props;
    const date = e.target.value;
    const dataValue = new Date(date);
    setCurrentNoticia({ ...noticia, dataPublicacao: dataValue.toString() });
    console.log(noticia.dataPublicacao);
  }

  handleTituloChande(e: any) {
    const titulo = e.target.value;
    const { noticia, setCurrentNoticia } = this.props;
    setCurrentNoticia({ ...noticia, titulo });
    console.log(noticia.titulo);
  }

  render() {
    let titulo = null;
    let data = null;
    const { mode } = this.props;
    const { noticia } = this.props;
    const dataValue = new Date(noticia.dataPublicacao);

    if (mode === PageMode.VISUALISATION) {
      titulo = <div className="title-header">{noticia.titulo}</div>;
      data = <i className="date">{`${dataValue.getDate()}/${dataValue.getMonth()}/${dataValue.getFullYear()}`}</i>;
    } else {
      titulo = <input type="text" className="title-header" onChange={e => this.handleTituloChande(e)} defaultValue={noticia.titulo} />;
      data = <input type="date" onChange={e => this.handleDateChande(e)} />;
    }

    return (
      <div className="content">
        {titulo}
        {data}
        {this.getBotoes(mode)}
      </div>
    );
    }
}

const mapStateToProps = (state: ApplicationState) => ({
  mode: state.repositories.mode,
  noticia: state.repositories.currentNoticia,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(NoticiaActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ArtigoHeader);
