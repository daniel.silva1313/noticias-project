import { combineReducers } from 'redux';

import repositories from './noticias';

export default combineReducers({
  repositories,
});
