import { all, takeLatest } from 'redux-saga/effects';

import { NoticiaTypes } from './noticias/types';
import { load, saveOrUpdate, remove } from './noticias/sagas';

export default function* rootSaga() {
  return yield all([
    takeLatest(NoticiaTypes.LOAD_REQUEST, load),
    takeLatest(NoticiaTypes.SAVE_OR_UPDATE, saveOrUpdate),
    takeLatest(NoticiaTypes.REMOVE, remove),
  ]);
}
