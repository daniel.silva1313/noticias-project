import { call, put } from 'redux-saga/effects';
import api from '../../../services/api';

import {
 loadSuccess,
 loadFailure,
 add,
 removeItem,
} from './actions';

export function* load() {
  try {
    const response = yield call(api.get, 'noticias');
    yield put(loadSuccess(response.data));
  } catch (err) {
    yield put(loadFailure());
  }
}

export function* saveOrUpdate(noticia: any) {
  try {
    const response = yield call(api.post, 'noticias', noticia);
    yield put(add(response.data));
  } catch (err) {
    yield put(loadFailure());
  }
}

export function* remove(id: any) {
  try {
    yield call(api.delete, `noticias/${id.payload}`);
    yield put(removeItem(id.payload));
  } catch (err) {
    yield put(loadFailure());
  }
}
