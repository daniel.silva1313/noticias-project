import { Reducer } from 'redux';
import {
 NoticiaState, NoticiaTypes, Noticia, PageMode,
} from './types';

const INITIAL_STATE: NoticiaState = {
  data: [],
  currentNoticia: {
    id: undefined,
    titulo: 'New titulo',
    conteudo: 'New conteudo',
    dataPublicacao: '',
  },
  mode: PageMode.VISUALISATION,
  error: false,
  loading: false,
};

const reducer: Reducer<NoticiaState> = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case NoticiaTypes.LOAD_REQUEST:
      return { ...state, loading: true };
    case NoticiaTypes.LOAD_SUCCCES:
      return {
      ...state, loading: false, error: false, data: action.payload.data,
      };
    case NoticiaTypes.LOAD_FAILURE:
      return {
      ...state, loading: false, error: true, data: [],
      };
    case NoticiaTypes.ADD:
      // se já tiver alguma inserieda
      if (state.data.findIndex(not => not.id === action.payload.id) !== -1) {
        return {
          ...state,
          data: [
            ...state.data.map<Noticia>((not) => {
              if (not.id === action.payload.id) {
                return action.payload;
              } return not;
            }),
          ],
        };
      }
      return { ...state, data: [...state.data, action.payload] };
    case NoticiaTypes.REMOVE:
      if (state.data.findIndex(not => not.id === action.payload.id) === -1) {
        return {
          ...state,
          data: [
            ...state.data.filter(not => not.id !== action.payload),
          ],
        };
      }
    return { ...state };
    case NoticiaTypes.SAVE_OR_UPDATE:
      return { ...state };
    case NoticiaTypes.SET_CURRENT:
      return { ...state, currentNoticia: action.payload };
    case NoticiaTypes.CHANGE_PAGEMODE:
      return { ...state, mode: action.payload };
    default:
      return state;
  }
};

export default reducer;
