/**
 * Action types
 */
export enum NoticiaTypes {
  LOAD_REQUEST = '@noticias/LOAD_REQUEST',
  LOAD_SUCCCES = '@noticias/LOAD_SUCCCES',
  LOAD_FAILURE = '@noticias/LOAD_FAILURE',
  SET_CURRENT= '@noticias/SET_CURRENT',
  SAVE_OR_UPDATE = '@noticias/SAVE_OR_UPDATE',
  ADD = '@noticias/ADD',
  REMOVE = '@noticias/REMOVE',
  CHANGE_PAGEMODE = '@page/CHANGE_PAGEMODE'
}

/**
 * Data types
 */
export interface Noticia {
  id?: string;
  titulo: string;
  conteudo: string;
  dataPublicacao: string;
}

/**
 * State type
 */
export interface NoticiaState {
  readonly data: Noticia[]
  readonly currentNoticia: Noticia
  readonly mode: PageMode
  readonly loading: boolean
  readonly error: boolean
}

export enum PageMode {
  CREATE_UPDATE = '@create_update',
  VISUALISATION = '@visualisation'
}
