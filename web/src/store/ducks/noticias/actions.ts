/* eslint-disable linebreak-style */
import { action } from 'typesafe-actions';
import { NoticiaTypes, Noticia, PageMode } from './types';


export const loadRequest = () => action(NoticiaTypes.LOAD_REQUEST);

export const loadSuccess = (data: Noticia[]) => action(NoticiaTypes.LOAD_SUCCCES, { data });

export const loadFailure = () => action(NoticiaTypes.LOAD_FAILURE);

export const setCurrentNoticia = (noticia: Noticia) => action(NoticiaTypes.SET_CURRENT, noticia);

export const saveOrUpdate = (noticia: Noticia) => action(NoticiaTypes.SAVE_OR_UPDATE, noticia);

export const add = (noticia: Noticia) => action(NoticiaTypes.ADD, noticia);

export const removeItem = (id: string) => action(NoticiaTypes.REMOVE, id);

export const changePageMode = (mode: PageMode) => action(NoticiaTypes.CHANGE_PAGEMODE, mode);
