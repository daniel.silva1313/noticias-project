
import { Repository } from '../common/Repository'
import { Noticia } from '../entities/noticia/noticia'

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface NoticiasRepository extends Repository<Noticia> {
}
