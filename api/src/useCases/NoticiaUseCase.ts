import { Noticia } from '../entities/noticia/noticia'

export default interface NoticiaUseCase{
    excute(data: any):Promise<Noticia[]| Noticia >;
}
