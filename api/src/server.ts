import app from './app'

require('dotenv').config()
const PORT = process.env.SERVER_PORT
app.listen((PORT || 3002), async () => {
  console.log(`Express started at http://localhost:${(PORT || 3002)}`)
})
