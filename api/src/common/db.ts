import mongoose from 'mongoose'

require('dotenv').config()
const { DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME } = process.env

export const connect = async () : Promise<void> => {
  const connetString = (DB_USER)
    ? `mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`
    : `mongodb://nodefault:nodefault@localhost:27017/nodefault?authSource=admin`
  mongoose.connect(
    connetString,
    {
      // useNewUrlParser: true,
    }
  )
  // mongoose.set('useNewUrlParser', true);
  mongoose.connection.on('error', () => console.error('connection error:'))
  mongoose.connection.once('open', () => console.log('database connected'))
}
export const close = (): Promise<void> => mongoose.connection.close()
