export interface Repository<T> {
    findById(id: string): Promise<T>;
    getAll(): any;
    getByQuery(dataInit?: string,
        dataEnd?: string,
        indexInit?: number,
        count?: number): Promise<T[]>;
    add(T: T): Promise<T>;
    update(id: string, T: T): Promise<T>;
    delete(id: string): Promise<T>
}
