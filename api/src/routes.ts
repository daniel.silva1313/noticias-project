import { Router } from 'express'
import CreateUpdateNoticiaController from './controllers/CreateUpdateNoticiaController'
import DeleteNoticiaController from './controllers/DeleteNoticiaController'
import ListNoticiaController from './controllers/ListNoticiaController'

const routes = Router()

routes.get('/noticias', ListNoticiaController.handle)
routes.post('/noticias', CreateUpdateNoticiaController.handle)
routes.delete('/noticias/:id', DeleteNoticiaController.handle)

export default routes
