
export class Noticia {
    public id?: string;

    public titulo!: string;
    public conteudo!: string;
    public dataPublicacao!: Date;

    public constructor (props: Noticia) {
      Object.assign(this, props)
      this.dataPublicacao = new Date()
    }
}
