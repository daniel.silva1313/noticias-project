import mongoose from 'mongoose'

const schema = new mongoose.Schema(
  {
    _id: {
      _id: true,
      name: 'id',
      auto: true,
      type: mongoose.Types.ObjectId,
      required: false
    },

    titulo: {
      type: String,
      required: true
    },

    conteudo: {
      type: String,
      required: true
    },

    dataPublicacao: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true,
    toJSON: { virtuals: true, getters: true },
    toObject: { virtuals: true, getters: true }
  }
)
export const NoticiaModel = mongoose.model('Noticia', schema)
