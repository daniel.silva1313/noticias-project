import { Request, Response } from 'express'
import { NoticiaModel } from '../entities/noticia/schema'
import { Noticia } from '../entities/noticia/noticia'

class CreateUpdateNoticiaController {
  public async handle (request: Request, response: Response): Promise<Response> {
    const { id,
      titulo,
      conteudo,
      dataPublicacao }: Noticia = request.body.payload
    try {
      const noticiaAreadyExist = await NoticiaModel.findById(id)

      if (noticiaAreadyExist) {
        noticiaAreadyExist.set('titulo', titulo)
        noticiaAreadyExist.set('conteudo', conteudo)
        noticiaAreadyExist.set('dataPublicacao', dataPublicacao)
        await NoticiaModel.findByIdAndUpdate(id, noticiaAreadyExist)
        return response.json(await NoticiaModel.findById(id))
      }

      const res = await NoticiaModel.create({ titulo, conteudo, dataPublicacao: Date() })
      return response.json(res)
    } catch (err) {
      response.status(500).json(err)
      throw err
    }
  }
}
export default new CreateUpdateNoticiaController()
