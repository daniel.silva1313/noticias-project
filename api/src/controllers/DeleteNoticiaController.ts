import { Request, Response } from 'express'
import { NoticiaModel } from '../entities/noticia/schema'

class DeleteNoticiaController {
  public async handle (request: Request, response: Response): Promise<Response> {
    try {
      const { id } = request.params
      const noticiaAreadyExist = await NoticiaModel.findById(id)
      if (noticiaAreadyExist) {
        await NoticiaModel.findByIdAndDelete(id)
        return response.json(await NoticiaModel.findById(id))
      }
    } catch (err) {
      response.status(500).json(err)
      throw err
    }
  }
}
export default new DeleteNoticiaController()
