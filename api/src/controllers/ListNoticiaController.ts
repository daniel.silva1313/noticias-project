import { Request, Response } from 'express'
import { NoticiaModel } from '../entities/noticia/schema'
// import { inject, injectable } from 'inversify'

class ListNoticiaController {
  public async handle (request: Request, response: Response): Promise<Response> {
    const noticias = await NoticiaModel.find()

    return response.json(noticias)
  }
}
export default new ListNoticiaController()
