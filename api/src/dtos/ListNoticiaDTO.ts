export interface ListNoticiaDTO {
    id?: string;
    titulo?: string;
    dateInit?: string;
    dateEnd?: string;
    indexInit?: number;
    count?: number;
}
